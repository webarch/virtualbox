This is based on the [Linux install instructions](https://www.virtualbox.org/wiki/Linux_Downloads).

## Install VirtualBox locally

Clone the repo and run the install script (using `sudo`):

```bash
git clone https://git.coop/webarch/virtualbox.git
cd virtualbox
./install.sh
```

## Ansible Galaxy

This repo should also be able to be used as an Ansible Galaxy role since the `roles` directory only contains symlinks.

Add the following to your `requirements.yml`:

```yml
- name: virtualbox
  src: https://git.coop/webarch/virtualbox.git
  version: master
  scm: git
```

And run:

```bash
ansible-galaxy install -r requirements.yml --force
```


